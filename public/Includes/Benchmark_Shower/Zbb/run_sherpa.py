from subprocess import Popen as run
from subprocess import check_output
from sys import stdin, stdout, stderr
import os
import argparse

parser = argparse.ArgumentParser(description='description')
parser.add_argument('-n','--name',type=str,dest='name')
parser.add_argument('--dir',default='/nfs/farm/g/theory/u1/dnapo/trunk_clean',
                    type=str, help='sherpa executable dir',dest='sherpa_dir')
parser.add_argument('-s', '--sherpa-extra-args',help='sherpa extra args, to be passed in inverted commas',
                    default='',type=str,dest='sherpa_extra_args')
parser.add_argument('-nj','--njobs',type=int,default=20,dest='njobs')
parser.add_argument('-i','--init',type=int,default=0,dest='init')
parser.add_argument('-e',type=str,default='1M',dest='events')
parser.add_argument('-a','--anal-dir',type=str,default='Analysis',dest='anal_dir')
args = parser.parse_args()
sherpa_exe  = args.sherpa_dir + '/bin/Sherpa'
sherpa_args = args.sherpa_extra_args.split(' ')

shower_gens      = ['CSS', 'Dire']

commit_hash = check_output(['git', 'rev-parse', '--short', 'HEAD'],
                           cwd=args.sherpa_dir).strip('\n')

def seeds(init, jobs):
    return range(init,init+jobs,1)

def run_sherpa(sg,commit_hash,evol_scheme='def'):
    subprocs = []
    command  = [sherpa_exe,'SHOWER_GENERATOR=' + sg] + sherpa_args
    if(evol_scheme!='def'):
        command.append('CSS_EVOLUTION_SCHEME=' + evol_scheme)
    command.append('-e' + args.events)
    for rs in seeds(args.init,args.njobs):
        logname = 'log_' + sg + '_' + evol_scheme + '_' + str(rs) 
        if(os.path.exists(logname)):
            os.remove(logname)
        execu = command + ['RANDOM_SEED=' + str(rs),
                           'ANALYSIS_OUTPUT='+args.anal_dir + '/' + args.name + '_' + sg
                           +'_evol_' + evol_scheme + '_rev_'
                           + commit_hash + '_rs_' + str(rs), 'LOG_FILE='+ logname]
        print(execu)
        p = run(execu,stdin=stdin,stdout=stdout,stderr=stderr)
        subprocs.append(p)

    return subprocs

def check_status(subprocs):
    running = True
    while running:
        temp = 0
        for sp in subprocs:
            if(sp.poll()==None):
                temp += 0
            elif(sp.poll()==0):
                temp += 1
        #if(temp==len(subprocs)):
        if(temp > len(subprocs)/2):
            running=False

for sg in shower_gens:
    if(sg=='CSS'):
        subprocs = []
        shower_css_evol  = ['def','1', '2', '3']
        for evol_scheme in shower_css_evol:
            rs = run_sherpa(sg,commit_hash,evol_scheme)
            subprocs = subprocs + rs
            check_status(subprocs)

    elif(sg=='Dire'):
        rs = run_sherpa(sg,commit_hash)
        check_status(rs)
