Dire="${1}_Dire_evol_def_rev_05396e4.yoda"
CSS0="${1}_CSS_evol_def_rev_05396e4.yoda"
CSS1="${1}_CSS_evol_1_rev_05396e4.yoda"
CSS2="${1}_CSS_evol_2_rev_05396e4.yoda"
CSS3="${1}_CSS_evol_3_rev_05396e4.yoda"


# rivet command

rivet-mkhtml --mc-errs\
	     ${Dire}:Title="Dire NLO"\
             ${CSS0}:Title="CSS scheme 0"\
	     ${CSS1}:Title="CSS scheme 1"\
	     ${CSS2}:Title="CSS scheme 2"\
	     ${CSS3}:Title="CSS scheme 3"

